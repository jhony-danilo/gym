module Base
  class FormBuilder < SimpleForm::FormBuilder
    map_type :decimal, :float, to: Inputs::DecimalInput
    map_type :string, :tel, to: Inputs::StringInput
    map_type :integer, to: Inputs::NumericInput
    map_type :date, to: Inputs::DateInput
    map_type :time, to: Inputs::TimeInput
    map_type :datetime, to: Inputs::DateTimeInput
    map_type :modal_field, to: Inputs::ModalInput
    map_type :radio_buttons, to: Inputs::RadioButtonLabelInput
    map_type :text, to: Inputs::TextInput
    map_type :file, to: Inputs::FileInput

    def back_button(value = nil, options = {})
      value, options = nil, value if value.is_a?(Hash)

      value ||= template.translate('.back', cascade: true)

      options[:class] = "#{options[:class].join(' ')} secondary".strip
      options[:href] ||= template.smart_collection_path
      options[:id] ||= "#{object_name}_back"

      template.link_to(
        "<i class='icon-arrow-left bigger-110'></i>".html_safe + value,
        options.delete(:href),
        options
      )
    end

    def input(attribute_name, options = {}, &block)
      options[:input_html] ||= { dimension: "#{options[:dimension]}" } if options[:dimension]

      reflection = find_association_reflection(attribute_name)

      if reflection && !reflection.options[:polymorphic]
        options[:label_method] = :to_s unless options[:label_method]

        attribute_name = reflection.options[:foreign_key] || :"#{reflection.name}_id"

        klass_name = reflection.class_name

        if options[:shortcut_register]
          path = Controller.new_resource_path(klass_name)
          options[:input_html] ||= { link_to_new: path + '?shortcut_register=true' }
        end

        model = klass_name.to_s.camelize.constantize

        if model.respond_to?(:active_filter)
          collection = model.active_filter.ordered
        else
          if options[:input_html]
            collection = model.ordered unless options[:input_html][:not_collection]
          else
            collection = model.ordered
          end
        end

        options.reverse_merge!(as: :select, collection: collection)
      elsif collection = find_enumeration_reflection(attribute_name)
        options.reverse_merge!(as: :select, collection: collection)
      end

      super
    end

    def find_enumeration_reflection(attribute_name)
      object.class.enumerations[attribute_name.to_sym] if object.class.respond_to?(:enumerations)
    end
  end
end
