module Inputs
  class DecimalInput < SimpleForm::Inputs::Base
    include ActionView::Helpers::NumberHelper

    def input(wrapper_options)
      @builder.text_field(attribute_name, input_html_options)
    end

    protected

    def input_html_classes
      super.unshift('string form-control')
    end

    def input_html_options
      super.tap do |options|
        options[:type] ||= :text
        options[:maxlength] ||= maxlength
        options[:data] ||= {}

        options[:value] = number_with_precision(
          options[:value].to_f,
          precision: precision
        ) if options[:value]

        options[:data][:decimal] ||= ','
        options[:data][:thousands] ||= '.'
        options[:data][:prefix] ||= 'R$ '
        options[:data][:precision] ||= precision
        options[:data][:negative] ||= negative
      end
    end

    def maxlength
      return unless column_with_precision_and_scale?

      points, missing = (column.precision - column.scale).divmod(3)

      if missing.zero?
        column.precision + points
      else
        column.precision + points + 1
      end
    end

    def precision
      return unless column_with_precision_and_scale?

      column.scale
    end

    def negative
      if options.has_key?(:negative)
        options[:negative]
      else
        false
      end
    end

    private

    def column_with_precision_and_scale?
      column && column.precision && column.scale
    end
  end
end
