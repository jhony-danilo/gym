module Inputs
  class TextInput < SimpleForm::Inputs::TextInput

    protected

    def input_html_options
      super.tap do |options|
        options[:size] ||= '50x5'
      end
    end
  end
end
