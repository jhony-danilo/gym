class CreateTableEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.attachment :picture
      t.boolean :active
      t.timestamps
    end
  end
end
