class CreatePlanValues < ActiveRecord::Migration
  def change
    create_table :plan_values do |t|
      t.decimal :value, null: false, precision: 10, scale: 2
      t.integer :modality_id, null: false
      t.integer :plan_id, null: false
      t.timestamps
    end
    add_foreign_key :plan_values, :modality_id, :modalities
    add_foreign_key :plan_values, :plan_id, :plans
  end
end
