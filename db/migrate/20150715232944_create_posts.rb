class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :name, null: false
      t.boolean :manager, default: false
      t.boolean :active, default: false
      t.references :department, null: false
      t.timestamps
    end
    add_foreign_key :posts, :department_id, :departments
  end
end
