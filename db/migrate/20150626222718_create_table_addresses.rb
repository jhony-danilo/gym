class CreateTableAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.integer :owner_id, null: false
      t.string :owner_type, null: false
      t.string :street
      t.string :complement
      t.string :number
      t.string :district
      t.string :zipcode
      t.integer :city_id
      t.integer :state_id
    end
    add_foreign_key :addresses, :city_id, :cities
    add_foreign_key :addresses, :state_id, :states
  end
end