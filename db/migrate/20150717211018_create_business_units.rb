class CreateBusinessUnits < ActiveRecord::Migration
  def change
    create_table :business_units do |t|
      t.string :name, null: false
      t.string :company_name, null: false
      t.string :cnpj, null: false
      t.attachment :document_cnpj
      t.attachment :logo
      t.string :ie
      t.string :im
      t.timestamps
    end
  end
end
