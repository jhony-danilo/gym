class CreateTablePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :name, null: false
      t.integer :related_id, null: false
      t.string :related_type, null: false
      t.string :cpf, null: false
      t.string :gender, null: false
      t.string :civil_status
      t.string :rg, null: false
      t.string :rg_organ, null: false
      t.string :father_name
      t.string :mother_name, null: false
      t.string :schooling
      t.date :birth_date, null: false
      t.timestamps
    end
  end
end
