class CreateTablePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.string :number, null: false, limit: 12
      t.string :classification
      t.integer :related_id, null: false, index: true
      t.string :related_type, null: false, index: true

      t.timestamps
    end
  end
end
