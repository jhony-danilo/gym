class AddColumnToStudents < ActiveRecord::Migration
  def change
    add_column :students, :schedule, :time
  end
end
