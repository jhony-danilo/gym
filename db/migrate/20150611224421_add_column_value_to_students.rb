class AddColumnValueToStudents < ActiveRecord::Migration
  def change
    add_column :students, :value, :decimal, precision: 10, scale: 2, default: 0
  end
end
