class CreateBillingPeriods < ActiveRecord::Migration
  def change
    create_table :billing_periods do |t|
      t.string :name, null: false
      t.integer :months_qtt, null: false
      t.timestamps
    end
  end
end
