class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.time :horary, null: false
      t.string :week_day, null: false
      t.integer :plan_value_id
      t.timestamps
    end
    add_foreign_key :schedules, :plan_value_id, :plan_values
  end
end
