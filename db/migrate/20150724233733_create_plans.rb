class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.string :name, null: false
      t.integer :billing_period_id, null: false
      t.boolean :active
      t.timestamps
    end
    add_foreign_key :plans, :billing_period_id, :billing_periods
  end
end
