Rails.application.routes.draw do
  devise_for :users

  scope 'client', module: 'client', as: 'client' do
    resources :students
  end

  scope 'organization', module: 'organization', as: 'organization' do
    resources :states, only: [:cities] do
      get :cities
    end
    resources :departments do
      resources :posts
    end
    resources :employees
    resources :plans
    resources :business_units
  end

  scope 'settings', module: 'settings', as: 'settings' do
    resources :schedules
    resources :modalities
    resources :billing_periods
  end

  root :to => 'home#index'
end
