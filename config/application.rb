require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Gym
  class Application < Rails::Application
    config.to_prepare { Devise::SessionsController.layout 'login' }
    config.autoload_paths += %W(
    #{config.root}/lib
    )

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.i18n.load_path += Dir["#{config.root}/config/locales/**/*.yml"]
    config.i18n.enforce_available_locales = false
    config.i18n.available_locales = ["pt-BR"]
    config.i18n.default_locale = :'pt-BR'
    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    # Include helpers from current controller only
    config.action_controller.include_all_helpers = false

    config.autoload_paths << "#{Rails.root}/app/enumerations"

    css_files = []
    Dir.glob("#{config.root}/app/assets/stylesheets/*.{css, erb}").each { |f|
      css_files << File.basename(f)
    }

    js_files = []
    Dir.glob("#{config.root}/app/assets/javascripts/*.{js, erb}").each { |f|
      js_files << File.basename(f)
    }
    config.assets.precompile += css_files.concat(js_files)

    # Add Bower components to assets pipeline
    config.assets.paths << Rails.root.join('vendor', 'assets', 'bower_components', 'angular')

  end
end
