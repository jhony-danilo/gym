# Sobrescrito para criar o atributo action_list, que permite acessar o que foi
# definido no controller em 'actions'. Isso será usado nas permissões para
# extrair quais são utilizadas
module InheritedResources
  module ClassMethods
    ACTIONS = [ :index, :show, :new, :edit, :create, :update, :destroy, :activate, :inactivate ]

    attr_accessor :informed_actions

    def action_list
      informed_actions || ACTIONS
    end

    protected

    # Defines wich actions will be inherited from the inherited controller.
    # Syntax is borrowed from resource_controller.
    #
    #   actions :index, :show, :edit
    #   actions :all, :except => :index
    #
    def actions(*actions_to_keep)
      raise ArgumentError, 'Wrong number of arguments. You have to provide which actions you want to keep.' if actions_to_keep.empty?

      @informed_actions = actions_to_keep

      options = actions_to_keep.extract_options!
      actions_to_remove = Array(options[:except])
      actions_to_remove += ACTIONS - actions_to_keep.map { |a| a.to_sym } unless actions_to_keep.first == :all
      actions_to_remove.map! { |a| a.to_sym }.uniq!
      (instance_methods.map { |m| m.to_sym } & actions_to_remove).each do |action|
        undef_method action, "#{action}!"
      end
    end
  end
end
