FactoryGirl.define do
  factory :user do
    email 'taveira@hotmail.com'
    password '12345678'
    login 'user'
    administrator false

    factory :admin do
      administrator true
    end
  end
end