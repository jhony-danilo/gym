require 'spec_helper'

feature 'User' do
  background do
    sign_in
  end

  scenario 'update' do
    click_link current_user.to_s.capitalize

    click_link 'Perfil'

    expect(page).to have_field  'Email', with: 'taveira@hotmail.com'

    fill_in 'Email', with: 'jhony.taveira@hotmail.com'
    fill_in 'Senha atual', with: '12345678'

    check 'user_changed'

    fill_in 'Senha', with: '12341234'
    fill_in 'Confirmação de senha', with: '12341234'

    fill_in 'Usuário', with: 'taveira'

    click_button 'Atualizar'

    expect(page).to have_content 'Você atualizou sua conta com sucesso.'
    expect(page).to have_field  'Usuário', with: 'taveira'
    expect(page).to have_field  'Email', with: 'jhony.taveira@hotmail.com'

    current_user.reload

    click_link current_user.to_s.capitalize
    click_link 'Sair'

    fill_in 'Usuário', with: 'taveira'
    fill_in 'Senha', with: '12341234'

    click_button 'Entrar'

    expect(page).to have_content 'Logado com sucesso.'
  end
end
