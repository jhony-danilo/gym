var gym = angular.module('gym', []);

gym.controller('stateController', ['$scope', '$http', '$timeout', function($scope, $http, $timeout){
  $scope.getCitiesByStateId = function(){
    var stateId = $scope.stateId;
    collection(stateId);
  };

  function collection(stateId){
    $('#city').removeClass('hide');
    $http.get('/organization/states/' + stateId + '/cities').success(function(data){
      $scope.cities = data;
    });
  }

  angular.element(document).ready(function(){
    if($('legend').text().split(' ')[2] == 'Editar'){
      collection($scope.state);
    }
  });

  $timeout(function(){
    $("select[name$='[state_id]'] option[selected]").prop('selected', true);
    $("select[name$='[city_id]'] option[value=" + $("select[name$='[city_id]']").attr('val') + "]").prop('selected', true)
  }, 500);
}]);
