//= require flatty/jquery/jquery.min
//= require flatty/jquery/jquery.mobile.custom.min
//= require flatty/jquery/jquery-migrate.min
//= require flatty/jquery/jquery-ui.min
//= require flatty/bootstrap/bootstrap.min
//= require flatty/plugins/plugins
//= require flatty/theme
//= require flatty/jquery/rails.js
//= require flatty/jquery/input_mask.js
//= require angular/index.js
//= require resource/index.js
//= require cocoon
//= require simple_form.validation
//= require cocoon_methods

bootboxDialog = function(message, callback){
  var options;
  options =
  {
    message: message,
    title: "Excluir",
    buttons: {
      danger: {
        label: "não",
        className: "btn-danger",
        callback: function(){
          return true;
        }
      },
      main: {
        label: "sim",
        className: "btn-primary",
        callback: function(){
          if(typeof callback === 'function'){
            return callback();
          }
        }
      }
    }
  }
  ;
  return bootbox.dialog(options);
}
;

$.rails.allowAction = function(element){
  var answer, callback, message;
  message = element.data("confirm");
  if(!message){
    return true;
  }
  answer = false;
  callback = void 0;
  if($.rails.fire(element, "confirm")){
    bootboxDialog(message, function(){
      var oldAllowAction;
      callback = $.rails.fire(element, "confirm:complete", [answer]);
      if(callback){
        oldAllowAction = $.rails.allowAction;
        $.rails.allowAction = function(){
          return true;
        };
        element.trigger("click");
        return $.rails.allowAction = oldAllowAction;
      }
    });
  }
  return false;
};

function addMask(){
  mask_phone($('.phone'));
  $('#date').mask("99/99/9999");
  $('.mask-cpf').mask("999.999.999-99");
  $("input[name$='[cnpj]']").mask("99.999.999/9999-99");
  $(function(){
    $('input.decimal').maskMoney();
  });
};

//altera o grid, quando for passar (ex: dimension: 5) pelo input
function addDimensionField(){
  $(".col-sm-2 select[dimension], input[dimension]").each(function(index, el){
    var dimension = el.getAttribute(['dimension']);

    if(($(this)[0].classList[1] == 'date') || ($(this)[0].classList[1] == 'time')){
      $(this).parent().parent().removeClass('col-sm-2').addClass('col-sm-' + dimension);

    }else{
      $(this).parent().removeClass('col-sm-2').addClass('col-sm-' + dimension);
    }
  });
}

$(function(){
  //remove masks
  $('form').submit(function(){
    $('.mask-cnpj').mask("99999999999999");
    $('.mask-cpf').mask("99999999999");
    unmask_phone($('.phone'));

    var value = $('input.decimal').maskMoney('unmasked');
    $('input.decimal').val(value[0]);
  });
  addDimensionField();
  addMask();
});
