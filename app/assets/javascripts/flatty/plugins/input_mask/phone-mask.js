function unmask_phone(field) {
  $.each(field, function(index, field) {
    field.value = field.value.replace(/[()\s-]/g,"")
  });
}

function mask_phone(field){
  $.each(field, function(index, field){
    field.maxLength = 15;
    setInterval(function(){
      field.value = field.value.replace(/\D/g, "");
      field.value = field.value.replace(/^(\d{2})(\d)/g, "($1) $2");
      field.value = field.value.replace(/(\d)(\d{4})$/, "$1-$2");
    }, 1);
  });
}
