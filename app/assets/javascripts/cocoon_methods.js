$('body').on('cocoon:after-insert', function (e, insertedItem) {
  $(document).ready(function () {
    var container = insertedItem.parent();
    setItemsQtt(container, getItemsQtt(container));
    addMask();
    //reloadModal();
  });
});

$('body').on('cocoon:before-remove', function (e, removedItem) {''
  $(document).ready(function () {
    var container = removedItem.parent();
    setItemsQtt(container, getItemsQtt(container) - 1);
    removedItem.css("display", "none");
  });
});

function getItemsQtt(container) {
  return container.children('div.nested-fields').size();
}

function setItemsQtt(container, qtt) {
  container.parents('div.widget-box').children('.widget-header').children('.widget-toolbar').children('.badge').html(qtt);
}

$(document).ready(function () {
  var container = $('.nested-fields').parent();
  setItemsQtt(container, getItemsQtt(container));
});
