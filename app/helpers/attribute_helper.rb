module AttributeHelper

  def show_attribute(attribute, options = {})
    object = options.delete(:object) || resource
    value = options.delete(:value)
    upcase = options.has_key?(:upcase) ? options.delete(:upcase) : true
    label = options.delete(:label) || localized_attribute(object.class, attribute)

    show = "<b>#{label}</b>: "
    if value
      show += upcase ? value.upcase : value
    else
      show += formatted_attribute(object, attribute, upcase: upcase)
    end
    show += '<br>'

    show.html_safe
  end

  def real_attribute(attribute)
    if attribute.to_s.end_with?('_id')
      attribute.to_s.split('_id').first
    else
      attribute
    end
  end

  def localized_attribute(klass, attribute_name)
    klass.human_attribute_name real_attribute(attribute_name)
  end

  def formatted_attribute(resource, attribute, options = {})
    upcase = options.has_key?(:upcase) ? options.delete(:upcase) : true

    attribute = real_attribute(attribute).to_s

    human_attr = attribute + '_humanize'
    if resource.respond_to?(human_attr)
      value = resource.send(human_attr) || ''
      (upcase ? value.upcase : value).html_safe
    else
      value_by_field_type(
        resource.send(attribute),
        attribute: attribute,
        upcase: upcase
      )
    end
  end

  def value_by_field_type(value, options = {})
    upcase = options.has_key?(:upcase) ? options.delete(:upcase) : true

    case value.class.to_s
      when 'String' then
        attribute = options.delete(:attribute)

        case attribute
          when 'zipcode' then
            Mask.zipcode(value)
          when 'cpf' then
            Mask.cpf(value)
          when 'cnpj' then
            Mask.cnpj(value)
          when 'phone1', 'phone2', 'phone3' then
            Mask.phone(value)
          else
            upcase ? value.mb_chars.upcase.to_s : value.capitalize
        end
      when 'Time' then
        Mask.date_time(value).split(',').second
      when 'ActiveSupport::TimeWithZone', 'Time' then
        Mask.date_time(value)
      when 'BigDecimal' then
        Mask.decimal(value)
      when 'Date' then
        Mask.date(value)
      when 'FalseClass' then
        "<i class='icon-remove-sign red'></i>".html_safe
      when 'TrueClass' then
        "<i class='icon-ok-sign green'></i>".html_safe
      else
        if value.to_s.nil?
          '-'
        else
          upcase ? value.to_s.upcase : value.to_s
        end
    end
  end

  def attributes
    attributes = resource_class.list_attributes
    attributes &= params[:attributes].split(',') if params[:attributes]

    associations = resource_class.reflect_on_all_associations

    attributes.map do |attribute|
      next if attribute.blank?
      association = associations.detect { |association| association.foreign_key == attribute }

      # If attribute is an association and is not polymorphic, use the association
      # name instead of the foreign key.
      if association
        association.name unless association.options.include? :polymorphic
      else
        attribute
      end
    end.compact
  end
end
