module CrudHelper
  include LinkHelper
  include AttributeHelper

  def nested(f, options={})
    color = options[:color]
    title = content_tag :div, class: 'title' do
      options[:title]
    end

    actions = content_tag :div,  class: 'actions' do
      link_to_remove_association nil, f
    end

    header = content_tag :div, class: 'box-header box-header-small ' + color, id: 'nested' do
      title + actions
    end

    box_toolbox = content_tag :div, class: 'box-toolbox box-toolbox-bottom' do
      '<br><br>'.html_safe
    end if options[:box_toolbox]

    body = content_tag :div, class: 'box-content box-padding' do
      yield
    end

    content_tag :div, class: 'box box-bordered muted-border box-nomargin nested-fields' do
      header + body + box_toolbox
    end
  end

  def list_table
    content_tag :div, class: 'box-content box-no-padding' do
      content_tag :div, class: 'responsive-table' do
        content_tag :div, class: 'scrollable-area' do
          yield
        end
      end
    end
  end

  def modal(options={})
    button = content_tag :button, 'aria-hidden' => 'true', class: 'close', 'data-dismiss' => 'modal', type: 'button' do
      '×'
    end

    h2 = content_tag :h2, class: 'modal-title' do
      if options.any?
        options[:title]
      else
        t "#{controller_name}." + params[:action], resource: singular.capitalize, cascade: true
      end
    end
    header = content_tag :div, class: 'modal-header' do
      button + h2
    end

    body = content_tag :div, class: 'modal-body' do
      content_tag :div, class: 'row' do
        content_tag :div, class: 'col-sm-12' do
          yield
        end
      end
    end
    header + body
  end

  def header(options)
    value_title = options.delete(:text)
    link_collapse = collapse if options.delete(:collapse)

    title = content_tag :div, class: 'title' do
      content_tag :strong do
        value_title
      end
    end

    content_tag :div, class: 'box-header box-header-small blue-background' do
      title + link_collapse
    end
  end

  def collapse
    link_1 = link_to raw("<i></i>"), '#', class: 'btn box-collapse btn-xs btn-link'
    link_2 = link_to raw("<i class='icon-remove'></i>"), '#', class: 'btn box-remove btn-xs btn-link'

    content_tag :div, class: 'actions' do
      link_1 + link_2
    end
  end

  def box_wrapper(options = nil)
    box_content = 'box-content ' + 'box-double-padding' if options.delete(:box_content_box_double_padding)
    box_content = 'box-content ' + 'box-padding' if options.delete(:box_content_box_padding)

    content = content_tag :div, class: box_content do
      yield
    end
    content_tag :div, class: 'row' do
      content_tag :div, class: 'col-sm-12' do
        content_tag :div, class: 'box' do
          header(options) + content
        end
      end
    end
  end

  def box
    content_tag :div, class: 'row' do
      content_tag :div, class: 'col-sm-12' do
        content_tag :div, class: 'box' do
          yield
        end
      end
    end
  end

  def plural(klass = resource_class)
    klass.model_name.human(count: 'many')
  end

  def singular(klass = resource_class)
    klass.model_name.human.mb_chars.upcase
  end

  def paginate(records = collection, options = {})
    will_paginate records, options if records.respond_to?(:total_pages)
  end

end
