module LinkHelper

  def controller_has_action?(act)
    controller.class.action_list.include? act
  end

  def create_link(options = {})
    return unless controller_has_action? :create

    str = t('buttons.new', resource: singular, cascade: true)
    path = options.delete(:path) || new_resource_path(search: params[:search])
    opts = options.delete(:opts) || {}

    if create?
      link_to(
        raw("<i class='icon-signin icon-white'></i> " + str),
        path,
        opts.merge(class: 'btn btn-md btn-primary')
      )
    end
  end

  def edit_btn(object, options = {})
    return unless controller_has_action? :edit

    link_to(
      raw("<i class='icon-pencil'></i>" + (t 'buttons.edit')),
      options.delete(:path) || edit_resource_path(object),
      class: 'btn btn-lg btn-warning',
      data: {'disable-with' => (t 'loading')}
    ) #if can? :update, resource
  end

  def back_btn(options = {})
    path = options.delete(:path) || smart_collection_path

    link_to(
      raw("<i class='icon-arrow-left'></i>" + (t 'back')),
      path,
      class: 'btn btn-lg btn-danger',
      data: {'disable-with' => (t 'loading')}
    )
  end

  def create?
    #&& can?(:create, controller_name)
    exist_route?(new_resource_path)
  rescue NoMethodError
    false
  end

  def exist_route?(route, environment = {})
    Rails.application.routes.recognize_path(route, environment)
    true
  rescue ActionController::RoutingError
    false
  end

  def edit_link(object, options = {})
    return unless controller_has_action? :edit

    #if can? :update, object
    link_to(
      raw("<i class='icon-edit'></i>"),
      options.delete(:path) || edit_resource_path(object),
      data: { placement: :top, rel: :tooltip },
      class: :green, title: (I18n.t 'actions.edit'), remote: options[:remote]
    )
    #end
  end

  def situation_change_link(object)
    return destroy_link(object) unless object.respond_to?(:activ)

    active = object.active if object.respond_to?(:active)

    data = {
      placement: :top,
      rel: :tooltip,
      confirm: (I18n.t '.are_you_sure')
    }

    if active
      link_to(
        raw("<i class='icon-minus bigger-130'></i>"),
        inactivate_resource_path(object),
        data: data,
        class: :red,
        title: (I18n.t 'actions.inactivate')
      ) if can? :inactivate, object
    else
      link_to(
        raw("<i class='icon-ok bigger-130'></i>"),
        activate_resource_path(object),
        data: data,
        class: :orange,
        title: (I18n.t 'actions.activate')
      ) if can? :activate, object
    end
  end

  def destroy_link(object, options = {})
    return unless controller_has_action? :destroy

    path = options.delete(:path) || resource_path(object)

    #if can? :manage, object
      link_to(
        raw("<i class='icon-remove-circle'></i>"),
        path,
        method: :delete,
        data: {
          placement: :top,
          rel: :tooltip,
          confirm: (I18n.t '.are_you_sure')
        },
        class: 'red tooltip-error',
        title: (I18n.t 'actions.delete'),
      )
    #end
  end

  def icons
    content_tag :div, class: 'action-buttons' do
      yield
    end
  end

  def icon_1
    'width=10px'
  end

  def icon_2
    'width=65px'
  end

  def icon_3
    'width=90px'
  end

  def icon_4
    'width=110px'
  end
end
