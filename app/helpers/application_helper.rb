module ApplicationHelper
  def simple_form_for(object, *args, &block)
    options = args.extract_options!
    options[:builder] ||= Base::FormBuilder
    super(object, *(args << options), &block)
  end

  def submenu(controller, modulu, options = {})
    @name = options.delete(:name) || (t "controllers.#{controller}")

    @path = options.delete(:path) || "#{modulu}_#{controller}_path"

    render 'layouts/submenu', { controller: controller }
  end

  def localized(collection_object)
    if block_given?
      collection_object.each { |object| yield(object.localized) }
    else
      collection_object.map { |object| object.localized }
    end
  end

  def controller_asset?
    Rails.application.assets.find_asset controller_name
  end
end
