class Organization::PostsController < CrudController
  belongs_to :department

  def new
    build_resource
    open_modal
  end

  def create
    create! { organization_department_path parent }
  end

  def edit
    open_modal
  end

  def update
    update! { organization_department_path parent }
  end

  def destroy
    destroy! { organization_department_path parent }
  end
end
