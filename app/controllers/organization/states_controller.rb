class Organization::StatesController < CrudController

  def cities
    render json: resource.cities.as_json
  end
end
