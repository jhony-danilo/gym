class CrudController < ApplicationController
  inherit_resources

  respond_to :js, :json

  has_scope :page, default: 1, only: [:index, :modal], unless: :disable_pagination?
  has_scope :per_page, default: 10, only: [:index, :modal], unless: :disable_pagination?

  def index(options={}, &block)
    params[:page] = 'all' unless params[:select_id].blank?
    params[:search] ||= {}
    build_resource
  end

  def update
    update! { edit_resource_path }
  end

  def destroy
    destroy! do |_, failure|
      failure.html { redirect_to collection_path, alert: errors(resource) }
    end
  end

  def smart_collection_path
    path = nil
    if respond_to? :index
      path ||= collection_path(params: extract_params(true)) rescue nil
    end
    if respond_to? :parent
      path ||= parent_path(params: extract_params(true)) rescue nil
    end
    path ||= root_path rescue nil
  end

  helper_method :smart_collection_path

  # Get collection using ordered scope
  def collection
    if params[:search].blank?
      return [] if resource_class.force_search? && request.xhr?

      get_collection_ivar || set_collection_ivar(collection_records.ordered)
    else
      searcher
    end
  end

  def collection_records
    if resource_class.respond_to?(:active_filter)
      apply_scopes end_of_association_chain.active_filter(params[:active] || true)
    else
      apply_scopes end_of_association_chain
    end
  end

  def disable_pagination?
    params[:page] == 'all'
  end

  # Build resource using I18n::Alchemy
  def build_resource
    get_resource_ivar || set_resource_ivar(effectively_build_resource)
  end

  # Effectively build resource using I18n::Alchemy
  def effectively_build_resource
    end_of_association_chain.send(method_for_build).tap do |object|
      object.localized.assign_attributes(*resource_params)
    end
  end

  def resource
    id = params[:id] || params[resource_class.model_name.to_s.underscore + '_id']
    get_resource_ivar || set_resource_ivar(end_of_association_chain.send(method_for_find, id))
  end
end
