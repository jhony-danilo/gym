class ApplicationController < ActionController::Base
  before_filter :authenticate_user!

  protect_from_forgery with: :exception

  helper_method :extract_params

  def extract_params(search = false)
    list = %w(action controller search utf8)
    list -= ['search'] if search
    params.dup.to_h.delete_if { |k| list.include?(k) }
  end

  def open_modal(view = params[:action])
    render partial: 'crud/open_modal', locals: { view: view }
  end
end
