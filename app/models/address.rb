class Address < ActiveRecord::Base
  attr_accessible :city_id, :state_id, :owner_id, :owner_type, :street, :complement,
    :number, :district, :zipcode

  validates :city_id, :state_id, presence: true

  belongs_to :owner, polymorphic: true
  belongs_to :city
  belongs_to :state
end
