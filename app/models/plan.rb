class Plan < ActiveRecord::Base
  belongs_to :billing_period

  has_many :plan_values

  accepts_nested_attributes_for :plan_values
  orderize
end
