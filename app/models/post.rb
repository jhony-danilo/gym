class Post < ActiveRecord::Base
  attr_accessible :name

  attr_list :name, :created_at

  belongs_to :department

  validates :name, presence: true

  orderize
end
