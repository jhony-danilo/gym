class PlanValue < ActiveRecord::Base
  attr_accessible :schedules_attributes

  belongs_to :modality
  has_many :schedules

  accepts_nested_attributes_for :schedules

  orderize
end
