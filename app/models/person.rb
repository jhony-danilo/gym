class Person < ActiveRecord::Base
  attr_accessible :name, :cpf, :rg, :rg_organ, :mother_name, :father_name,
    :schooling, :gender, :birth_date

  belongs_to :related, polymorphic: true
end
