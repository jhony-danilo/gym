class Employee < ActiveRecord::Base
  attr_accessible  :person_attributes, :user_attributes, :address_attributes, :picture,
    :phones_attributes

  attr_list :name, :picture

  has_one :person, as: :related
  has_one :user
  has_one :address, as: :owner

  has_many :phones, as: :related, dependent: :destroy

  has_attached_file :picture,
    url: "/anexos/:class/:attachment/:id/:style_:basename.:extension",
    path: ":rails_root/public/anexos/:class/:attachment/:id/:style_:basename.:extension"

  delegate :name, to: :person, allow_nil: true

  accepts_nested_attributes_for :person
  accepts_nested_attributes_for :user
  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :phones

  orderize :id
end
