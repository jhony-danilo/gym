class BusinessUnit < ActiveRecord::Base
  attr_accessible :name, :cnpj, :company_name, :address_attributes, :phones_attributes,
    :ie, :im

  attr_list :name, :company_name

  has_attached_file :document_cnpj,
    url: "/anexos/:class/:attachment/:id/:style_:basename.:extension",
    path: ":rails_root/public/anexos/:class/:attachment/:id/:style_:basename.:extension"

  has_attached_file :logo,
    url: "/anexos/:class/:attachment/:id/:style_:basename.:extension",
    path: ":rails_root/public/anexos/:class/:attachment/:id/:style_:basename.:extension"

  has_one :address, as: :owner
  has_many :phones, as: :related

  accepts_nested_attributes_for :phones, allow_destroy: true
  accepts_nested_attributes_for :address

  orderize
end
