class User < ActiveRecord::Base
  attr_accessible :login, :changed, :email, :password, :password_confirmation, :remember_me, :employee_id

  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable

  devise :timeoutable, timeout_in: 30.minutes

  validates :login, :password, :password_confirmation, presence: true

  belongs_to :employee

  attr_writer :changed
end
