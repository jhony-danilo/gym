class Schedule < ActiveRecord::Base
  attr_accessible :week_day, :horary

  belongs_to :plan_value

  has_enumeration_for :week_day, with: WeekDay

  orderize :id
end
