class Student < ActiveRecord::Base
  attr_accessible :name, :date_of_birth, :created_at, :schedule, :value, :total

  attr_list :schedule, :name, :date_of_birth, :value

  validates :name, presence: true
  validates :name, uniqueness: true

  orderize
end
