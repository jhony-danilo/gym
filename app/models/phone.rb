class Phone < ActiveRecord::Base
  attr_accessible :number

  belongs_to :related, polymorphic: true

  validates :number, presence: true
end
