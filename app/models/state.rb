class State < ActiveRecord::Base
  attr_accessible :acronym, :name, :capital_id

  has_many :cities

  belongs_to :capital, class_name: 'City'

  orderize
end
